const express = require("express");
const path = require("path");
const crypto = require("crypto");
const cors = require("cors");
const NodeRSA = require('node-rsa');
const bodyParser = require("body-parser");

// openssl list -cipher-algorithms

const app = express();

app.use(cors());
app.use(bodyParser.json());

// app.post("/encrypt", (req, res) => {
//   const { message, key, method } = req.body;
//   const cipher = crypto.createCipher(method, key);

//   let encrypted = cipher.update(message, "utf8", "hex");
//   encrypted += cipher.final("hex");

//   res.json({ data: encrypted });
// });

// app.post("/decrypt", (req, res) => {
//   const { encrypted, key, method } = req.body;
//   const decipher = crypto.createDecipher(method, key);

//   let decrypted = decipher.update(encrypted, "hex", "utf8");
//   decrypted += decipher.final("utf8");

//   res.json({ data: decrypted });
// });

const key = new NodeRSA({ b: 512 });
const publicKey = key.exportKey('public')
const privateKey = key.exportKey('private')
let encrypted

app.get('/generate', (req, res) => {
  res.json({ publicKey, privateKey })
})

app.post('/encrypt', (req, res) => {
  const { message } = req.body
  encrypted = key.encryptPrivate(message, 'base64')

  res.json({ data: encrypted })
})

app.post('/decrypt', (req, res) => {
  const { message } = req.body

  decrypted = key.decryptPublic(encrypted, 'utf8')
  res.json({ data: decrypted })
})

// const encrypted = key.encryptPrivate('Sometimes the same is different', 'base64')
// console.log(encrypted)

// const decrypted = key.decryptPublic(encrypted, 'utf8')
// console.log(decrypted)

const port = 3000;

app.listen(port, err => {
  if (err) throw err;

  console.log(`Running on localhost:${port}`);
});
